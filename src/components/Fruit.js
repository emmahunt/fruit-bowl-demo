import React from 'react';

function Fruit(props) {
  return (
    <div className="fruit">
        <img src={require('../img/' + props.name + '.jpg')}/>
        <p>
          Fruit name: {props.name}
        </p>
    </div>
  );
}

export default Fruit;
