import React, {useState, useEffect} from 'react';
import './App.css';

import Fruit from './components/Fruit.js';

function App() {

  const [fruitBowl, setFruit] = useState(['apple', 'pear', 'lemon', 'tomato', 'pear']);

  useEffect(() => {
    console.log('yum')
  },[])

  return (
    <div className="App">
      <header className="App-header">
        <p>
          This is a fruit bowl
          {fruitBowl.toLocaleString()}
        </p>
      </header>

    <button onClick = {()=>setFruit(fruitBowl.slice(0, fruitBowl.length - 1))}>
      Eat Fruit
    </button>

    <p>
      {fruitBowl}
    </p>

    {
    fruitBowl.map((fruit) => {
      return(
        <Fruit name = {fruit}/>
      )
    })
    }

    </div>
  );
}

export default App;
